import React, {Component} from 'react';
import TextField from 'material-ui/TextField';

import SearchResults from './SearchResults';

export default class SearchBar extends Component {
  
  render() {
    return (
      <div style={{margin: '15px', border: '1px solid lightGrey', paddingLeft: '10px'}}>
        <div className='search-bar-contents container'>
          <form>
            <div className='input-group'>
              <TextField
                id='searchValue'
                style={{backgroundColor: '#ffffff', width: '100%'}}
                underlineShow={false}
                placeholder='Search Pokèmon'
                className='search-field form-control'
                type='text'
                value={this.props.searchValue}
                onChange={(e, val) => this.props.updatePokemonSearch(val)}
              />
            
            </div>
          </form>
        </div>
        <SearchResults
          handleAddingPokemon={this.props.handleAddingPokemon}
          suggestedPokemon={this.props.suggestedPokemon}
        />
      </div>
    );
  }
}

