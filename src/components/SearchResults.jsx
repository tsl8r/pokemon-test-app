import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { Card } from 'material-ui/Card/index';

const SearchResults = (props) => {
  console.log(props)
  return (
    <div>
      {
        props.suggestedPokemon.map((pokemon, i) => {
          return (
            <div key={i} style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', fontFamily:'Roboto'}}>
              <p className='col-xs-12 col-sm-7 col-md-8'>
                {pokemon.name}
              </p>
              
              <div className='label-dropdown col-xs-12 col-sm-5 col-md-4'>
                <FlatButton label='Catch pokemon' onClick={() => props.handleAddingPokemon(pokemon.id)} />
              </div>
            </div>
          )
        })
      }
    </div>
  )
};

export default SearchResults;